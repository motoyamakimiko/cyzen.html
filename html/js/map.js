$(document).ready(function() {

  // Google Map
  var Map = null;
  // 中心のマーカー
  var CenterMarker = null;

  // UI init -------------------------------------------------------------------

  $('.panel').draggable({ containment: 'body', scroll: false });

  autosize($('textarea'));

  // Global Function -----------------------------------------------------------

  /**
   * URL パラメータを取得
   */
  var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;
    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
  };

  // ---------------------------------------------------------------------------

  var position = getUrlParameter('position');
  var positionLatiLong = position.split(',');

  Map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: Number(positionLatiLong[0]), lng: Number(positionLatiLong[1])},
    zoom: 13,
    mapTypeControl: false,
    draggable: false,
    navigationControl: false,
    disableDefaultUI: true,
    scrollwheel: false,
    navigationControl: false,
    mapTypeControl: false,
    scaleControl: false,
    draggable: false,
  });

  for (var i in MapDataArray) {
    var obj = MapDataArray[i];
    var marker = new google.maps.Marker({
      position: obj.position,
      map: Map,
      icon: 'img/num-icon/' + (Number(i) + 1) + '.png',

    });
    var infoWindow = new google.maps.InfoWindow({
      content: obj.html
    });
    infoWindow.open(map, marker);
    marker.addListener('click', function() {
      infoWindow.open(map, marker);
    });

    if (i == MapDataArray.length - 1) {
      setTimeout(function() {
        Map.setCenter({lat:33.2326422, lng: 131.6039034})
      }, 1000);
    }
  }

  google.maps.event.addListener(Map, 'center_changed', function(){
    var pos = Map.getCenter();
    if (CenterMarker == null) {
      CenterMarker = new google.maps.Marker({
        position: pos,
        map: Map,
        icon: 'img/centerMarker.png'
      });
    } else {
      setTimeout(function() {
        CenterMarker.setPosition(pos);
      }, 90)
    }
  });
});
