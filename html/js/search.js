$(document).ready(function() {

  // Google Map
  var Map = null;
  // 中心のマーカー
  var CenterMarker = null;

  // Datetimepicker

  $('.datetimepicker').datetimepicker({format: 'YYYY/MM/DD'});

  // Global Function -----------------------------------------------------------

  /**
   * Google Map Api 検索 フリーワードから検索
   * Mapのポジションも変更します
   */
  var doGeocode = function(address) {
    geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function(results, status){
      if(status == google.maps.GeocoderStatus.OK){
        Map.setCenter(results[0].geometry.location);
      }else{
        console.log('検索ワードに該当がありません：' + status);
      }
    });
  }

  // Event ---------------------------------------------------------------------

  /**
   * マップ検索 テキストフィールド
   * キーボード監視
   */
  $('.map-search-text').keypress( function ( e ) {
    // Enter Key
    if ( e.which == 13 ) {
      var address = $('.map-search-text').val();
      doGeocode(address);
    }
  });
  /**
   * マップ検索 テキストフィールド横ボタン
   */
  $('.map-search-btn').click(function(e) {
    var address = $('.map-search-text').val();
    doGeocode(address);
  });
  /**
   * 駐車場分類
   */
  $('.checkbox.all-check label').click(function(e) {
    $(this).parents('ul').find('input:checkbox').prop('checked', true);
  });
  /**
   * 競合他社
   */
  $('input.competitors').change(function(e) {
    if ($(this).is(':checked')) {
      $('.important-check-boxs').find('input:checkbox').prop('checked', true);
    } else {
      $('.important-check-boxs').find('input:checkbox').prop('checked', false);
    }
  });
  /**
   * 競合他社以外
   */
  $('input.other-than-competitors').change(function(e) {
    if ($(this).is(':checked')) {
      $('input.requiredReview').prop('checked', true);
      $('input.surveyed').prop('checked', true);
      $('input.internet-information-only').prop('checked', true);
    } else {
      $('input.requiredReview').prop('checked', false);
      $('input.surveyed').prop('checked', false);
      $('input.internet-information-only').prop('checked', false);
    }
  });
  /**
   * 調査日 カレンダー 表示
   */
  $('.datetimepicker_2').on('dp.show', function(e) {
  })
  /**
   * 調査日 カレンダー 非表示
   */
  $('.datetimepicker_2').on('dp.hide', function(e) {
  })
  /**
   * 調査日 カレンダー 変更
   */
  $('.datetimepicker').on('dp.change', function(e) {
    if (moment(e.date).isValid() == true) {
      $('.immediate-time-group input:radio').prop('checked', false);
    }
  })
  /**
   * 調査日 直近 nヶ月
   */
  $('.immediate-time-group input:radio').change(function(e) {
    $('.datetimepicker').val('');
  });

  // Map -----------------------------------------------------------------------

  Map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 35.6688792, lng: 139.7633672},
    zoom: 16,
    mapTypeControl: false
  });

  // サンプルのデータを元にマーカーを作成
  for (var i in MapDataArray) {
    var obj = MapDataArray[i];
    var marker = new google.maps.Marker({
      position: obj.position,
      map: Map
    });

    // InfoWindow
    var infoWindow = new google.maps.InfoWindow({
      content: obj.html
    });

    infoWindow.open(map, marker);

    marker.addListener('click', function() {
      infoWindow.open(map, marker);
      var customInfo = this.customInfo;
      $('.detail-wrap .detail').html(customInfo.html);
    });
    marker.customInfo = {};
    marker.customInfo.dataIndex = i;
    marker.customInfo.html = obj.html;

    if (i == MapDataArray.length - 1) {
      setTimeout(function() {
        Map.setCenter({lat:33.2326422, lng: 131.6039034})
      }, 1000);
    }
  }

  /**
   * Map Event
   */
  google.maps.event.addListener(Map, 'center_changed', function(){

    // 中心のマーカー
    var pos = Map.getCenter();
    if (CenterMarker == null) {
      CenterMarker = new google.maps.Marker({
        position: pos,
        map: Map,
        icon: 'img/centerMarker.png'
      });
    } else {
      setTimeout(function() {
        CenterMarker.setPosition(pos);
      }, 90)
    }
  });
});
