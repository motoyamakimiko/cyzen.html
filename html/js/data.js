var MapDataArray = [
  {
    id: 1,
    position: { lat: 33.2344639, lng: 131.6111607 },
    html: '<div><strong>1.屋内機会式</strong><br/><div class="detail">¥28,000 <span>空き: 有</span><br/>¥32,000 <span>空き: 有</span></div></div>',
    // タグ
    tag: '要再調査',
    // 駐車場名
    parkingLotName: '<a href="#">東京四ツ谷パーキングA</a>',
    // 最終調査日
    lastStudyDate: '2017/03/01',
    // 管理会社
    managementCompany: 'オフィスP（日本P）',
    // 総台数
    totalNumber: 200,
    // 内外
    insideAndOutside: '屋内',
    // 型式
    model: '機械式',
    // 車室判別名称
    cabinDiscriminationName: '大型HR',
    // 賃料
    rent: 100800,
    // 空き
    free: '有',
    // 全長
    fullLength: 5400,
    // 全幅
    fullWidth: 2300,
    // 全高
    totalHeight: 1800,
    // 重量
    weight: 2500,
    // 利用時間制限
    usageTimeLimit: 'MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字',
  },
  {
    id: 2,
    position: { lat: 33.2344639, lng: 131.6111607 },
    html: '<div><strong>2.屋内機会式</strong><br/><div class="detail">¥28,000 <span>空き: 有</span><br/>¥32,000 <span>空き: 有</span></div></div>',
    // タグ
    tag: '競合',
    // 駐車場名
    parkingLotName: '<a href="#">東京四ツ谷パーキングC</a>',

    // 最終調査日
    lastStudyDate: '2017/05/01',
    // 管理会社
    managementCompany: 'オフィスP（日本P）',
    // 総台数
    totalNumber: 320,
    // 内外
    insideAndOutside: '屋内',
    // 型式
    model: '機械式',
    // 車室判別名称
    cabinDiscriminationName: '大型HR',
    // 賃料
    rent: 100800,
    // 空き
    free: '有',
    // 全長
    fullLength: 5400,
    // 全幅
    fullWidth: 2300,
    // 全高
    totalHeight: 1800,
    // 重量
    weight: 2500,
    // 利用時間制限
    usageTimeLimit: 'MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字',
  },
  {
    id: 3,
    position: { lat: 33.2199403, lng: 131.5725829 },
    html: '<div><strong>3.屋内機会式</strong><br/><div class="detail">¥28,000 <span>空き: 有</span><br/>¥32,000 <span>空き: 有</span></div></div>',
    // タグ
    tag: '要再調査',
    // 駐車場名
    parkingLotName: '<a href="#">東京四ツ谷パーキングf</a>',
    // 最終調査日
    lastStudyDate: '2017/09/01',
    // 管理会社
    managementCompany: 'オフィスP（日本P）',
    // 総台数
    totalNumber: 200,
    // 内外
    insideAndOutside: '屋内',
    // 型式
    model: '機械式',
    // 車室判別名称
    cabinDiscriminationName: '大型HR',
    // 賃料
    rent: 100800,
    // 空き
    free: '有',
    // 全長
    fullLength: 5400,
    // 全幅
    fullWidth: 2300,
    // 全高
    totalHeight: 1800,
    // 重量
    weight: 2500,
    // 利用時間制限
    usageTimeLimit: 'MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字',
  },
  {
    id: 4,
    position: { lat: 33.2199403, lng: 131.5725829 },
    html: '<div><strong>4.屋内機会式</strong><br/><div class="detail">¥28,000 <span>空き: 有</span><br/>¥32,000 <span>空き: 有</span></div></div>',
    // タグ
    tag: '競合',
    // 駐車場名
    parkingLotName: '<a href="#">東京四ツ谷パーキングX</a>',
    // 最終調査日
    lastStudyDate: '2017/02/01',
    // 管理会社
    managementCompany: 'オフィスP（日本P）',
    // 総台数
    totalNumber: 3,
    // 内外
    insideAndOutside: '屋内',
    // 型式
    model: '機械式',
    // 車室判別名称
    cabinDiscriminationName: '大型HR',
    // 賃料
    rent: 100800,
    // 空き
    free: '有',
    // 全長
    fullLength: 5400,
    // 全幅
    fullWidth: 2300,
    // 全高
    totalHeight: 1800,
    // 重量
    weight: 2500,
    // 利用時間制限
    usageTimeLimit: 'MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字',
  },
  {
    id: 5,
    position: { lat: 33.2199403, lng: 131.5725829 },
    html: '<div><strong>5.屋内機会式</strong><br/><div class="detail">¥28,000 <span>空き: 有</span><br/>¥32,000 <span>空き: 有</span></div></div>',
    // タグ
    tag: '競合',
    // 駐車場名
    parkingLotName: '<a href="#">東京四ツ谷パーキング9</a>',
    // 最終調査日
    lastStudyDate: '2017/02/01',
    // 管理会社
    managementCompany: 'オフィスP（日本P）',
    // 総台数
    totalNumber: 10,
    // 内外
    insideAndOutside: '屋内',
    // 型式
    model: '機械式',
    // 車室判別名称
    cabinDiscriminationName: '大型HR',
    // 賃料
    rent: 100800,
    // 空き
    free: '有',
    // 全長
    fullLength: 5400,
    // 全幅
    fullWidth: 2300,
    // 全高
    totalHeight: 1800,
    // 重量
    weight: 2500,
    // 利用時間制限
    usageTimeLimit: 'MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字',
  },
  {
    id: 6,
    position: { lat: 33.2199403, lng: 131.5725829 },
    html: '<div><strong>1.屋内機会式</strong><br/><div class="detail">¥28,000 <span>空き: 有</span><br/>¥32,000 <span>空き: 有</span></div></div>',
    // タグ
    tag: '要再調査',
    // 駐車場名
    parkingLotName: '<a href="#">東京四ツ谷パーキングA</a>',
    // 最終調査日
    lastStudyDate: '2017/03/01',
    // 管理会社
    managementCompany: 'オフィスP（日本P）',
    // 総台数
    totalNumber: 20,
    // 内外
    insideAndOutside: '屋内',
    // 型式
    model: '機械式',
    // 車室判別名称
    cabinDiscriminationName: '大型HR',
    // 賃料
    rent: 100800,
    // 空き
    free: '有',
    // 全長
    fullLength: 5400,
    // 全幅
    fullWidth: 2300,
    // 全高
    totalHeight: 1800,
    // 重量
    weight: 2500,
    // 利用時間制限
    usageTimeLimit: 'MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字',
  },
  {
    id: 7,
    position: { lat: 33.2199403, lng: 131.5725829 },
    html: '<div><strong>2.屋内機会式</strong><br/><div class="detail">¥28,000 <span>空き: 有</span><br/>¥32,000 <span>空き: 有</span></div></div>',
    // タグ
    tag: '競合',
    // 駐車場名
    parkingLotName: '<a href="#">東京四ツ谷パーキングC</a>',

    // 最終調査日
    lastStudyDate: '2017/05/01',
    // 管理会社
    managementCompany: 'オフィスP（日本P）',
    // 総台数
    totalNumber: 20,
    // 内外
    insideAndOutside: '屋内',
    // 型式
    model: '機械式',
    // 車室判別名称
    cabinDiscriminationName: '大型HR',
    // 賃料
    rent: 100800,
    // 空き
    free: '有',
    // 全長
    fullLength: 5400,
    // 全幅
    fullWidth: 2300,
    // 全高
    totalHeight: 1800,
    // 重量
    weight: 2500,
    // 利用時間制限
    usageTimeLimit: 'MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字',
  },
  {
    id: 8,
    position: { lat: 33.2255414, lng: 131.5118749 },
    html: '<div><strong>3.屋内機会式</strong><br/><div class="detail">¥28,000 <span>空き: 有</span><br/>¥32,000 <span>空き: 有</span></div></div>',
    // タグ
    tag: '要再調査',
    // 駐車場名
    parkingLotName: '<a href="#">東京四ツ谷パーキングf</a>',
    // 最終調査日
    lastStudyDate: '2017/09/01',
    // 管理会社
    managementCompany: 'オフィスP（日本P）',
    // 総台数
    totalNumber: 20,
    // 内外
    insideAndOutside: '屋内',
    // 型式
    model: '機械式',
    // 車室判別名称
    cabinDiscriminationName: '大型HR',
    // 賃料
    rent: 100800,
    // 空き
    free: '有',
    // 全長
    fullLength: 5400,
    // 全幅
    fullWidth: 2300,
    // 全高
    totalHeight: 1800,
    // 重量
    weight: 2500,
    // 利用時間制限
    usageTimeLimit: 'MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字',
  },
  {
    id: 9,
    position: { lat: 33.2437102, lng: 131.4486842 },
    html: '<div><strong>4.屋内機会式</strong><br/><div class="detail">¥28,000 <span>空き: 有</span><br/>¥32,000 <span>空き: 有</span></div></div>',
    // タグ
    tag: '',
    // 駐車場名
    parkingLotName: '',
    // 最終調査日
    lastStudyDate: '',
    // 管理会社
    managementCompany: '',
    // 総台数
    totalNumber: '',
    // 内外
    insideAndOutside: '',
    // 型式
    model: '',
    // 車室判別名称
    cabinDiscriminationName: '',
    // 賃料
    rent: '',
    // 空き
    free: '',
    // 全長
    fullLength: '',
    // 全幅
    fullWidth: '',
    // 全高
    totalHeight: '',
    // 重量
    weight: '',
    // 利用時間制限
    usageTimeLimit: '',
  },
  {
    id:10,
    position: { lat: 33.0790125, lng: 131.7000615 },
    html: '<div><strong>5.屋内機会式</strong><br/><div class="detail">¥28,000 <span>空き: 有</span><br/>¥32,000 <span>空き: 有</span></div></div>',
    // タグ
    tag: '競合',
    // 駐車場名
    parkingLotName: '<a href="#">東京四ツ谷パーキング9</a>',
    // 最終調査日
    lastStudyDate: '2017/02/01',
    // 管理会社
    managementCompany: 'オフィスP（日本P）',
    // 総台数
    totalNumber: 20,
    // 内外
    insideAndOutside: '屋内',
    // 型式
    model: '機械式',
    // 車室判別名称
    cabinDiscriminationName: '大型HR',
    // 賃料
    rent: 100800,
    // 空き
    free: '有',
    // 全長
    fullLength: 5400,
    // 全幅
    fullWidth: 2300,
    // 全高
    totalHeight: 1800,
    // 重量
    weight: 2500,
    // 利用時間制限
    usageTimeLimit: 'MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字',
  },
  {
    id: 11,
    position: { lat: 33.0790125, lng: 131.7000615 },
    html: '<div><strong>1.屋内機会式</strong><br/><div class="detail">¥28,000 <span>空き: 有</span><br/>¥32,000 <span>空き: 有</span></div></div>',
    // タグ
    tag: '要再調査',
    // 駐車場名
    parkingLotName: '<a href="#">東京四ツ谷パーキングA</a>',
    // 最終調査日
    lastStudyDate: '2017/03/01',
    // 管理会社
    managementCompany: 'オフィスP（日本P）',
    // 総台数
    totalNumber: 20,
    // 内外
    insideAndOutside: '屋内',
    // 型式
    model: '機械式',
    // 車室判別名称
    cabinDiscriminationName: '大型HR',
    // 賃料
    rent: 100800,
    // 空き
    free: '有',
    // 全長
    fullLength: 5400,
    // 全幅
    fullWidth: 2300,
    // 全高
    totalHeight: 1800,
    // 重量
    weight: 2500,
    // 利用時間制限
    usageTimeLimit: 'MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字',
  },
  {
    id: 12,
    position: { lat: 33.0643397, lng: 131.7563665 },
    html: '<div><strong>2.屋内機会式</strong><br/><div class="detail">¥28,000 <span>空き: 有</span><br/>¥32,000 <span>空き: 有</span></div></div>',
    // タグ
    tag: '競合',
    // 駐車場名
    parkingLotName: '<a href="#">東京四ツ谷パーキングC</a>',

    // 最終調査日
    lastStudyDate: '2017/05/01',
    // 管理会社
    managementCompany: 'オフィスP（日本P）',
    // 総台数
    totalNumber: 20,
    // 内外
    insideAndOutside: '屋内',
    // 型式
    model: '機械式',
    // 車室判別名称
    cabinDiscriminationName: '大型HR',
    // 賃料
    rent: 100800,
    // 空き
    free: '有',
    // 全長
    fullLength: 5400,
    // 全幅
    fullWidth: 2300,
    // 全高
    totalHeight: 1800,
    // 重量
    weight: 2500,
    // 利用時間制限
    usageTimeLimit: 'MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字',
  },
  {
    id: 13,
    position: { lat: 33.0643397, lng: 131.7563665 },
    html: '<div><strong>3.屋内機会式</strong><br/><div class="detail">¥28,000 <span>空き: 有</span><br/>¥32,000 <span>空き: 有</span></div></div>',
    // タグ
    tag: '要再調査',
    // 駐車場名
    parkingLotName: '<a href="#">東京四ツ谷パーキングf</a>',
    // 最終調査日
    lastStudyDate: '2017/09/01',
    // 管理会社
    managementCompany: 'オフィスP（日本P）',
    // 総台数
    totalNumber: 20,
    // 内外
    insideAndOutside: '屋内',
    // 型式
    model: '機械式',
    // 車室判別名称
    cabinDiscriminationName: '大型HR',
    // 賃料
    rent: 100800,
    // 空き
    free: '有',
    // 全長
    fullLength: 5400,
    // 全幅
    fullWidth: 2300,
    // 全高
    totalHeight: 1800,
    // 重量
    weight: 2500,
    // 利用時間制限
    usageTimeLimit: 'MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字',
  },
  {
    id: 14,
    position: { lat: 33.0956403, lng: 131.6824722 },
    html: '<div><strong>4.屋内機会式</strong><br/><div class="detail">¥28,000 <span>空き: 有</span><br/>¥32,000 <span>空き: 有</span></div></div>',
    // タグ
    tag: '競合',
    // 駐車場名
    parkingLotName: '<a href="#">東京四ツ谷パーキングX</a>',
    // 最終調査日
    lastStudyDate: '2017/02/01',
    // 管理会社
    managementCompany: 'オフィスP（日本P）',
    // 総台数
    totalNumber: 20,
    // 内外
    insideAndOutside: '屋内',
    // 型式
    model: '機械式',
    // 車室判別名称
    cabinDiscriminationName: '大型HR',
    // 賃料
    rent: 100800,
    // 空き
    free: '有',
    // 全長
    fullLength: 5400,
    // 全幅
    fullWidth: 2300,
    // 全高
    totalHeight: 1800,
    // 重量
    weight: 2500,
    // 利用時間制限
    usageTimeLimit: 'MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字',
  },
  {
    id: 15,
    position: { lat: 33.0956403, lng: 131.6824722 },
    html: '<div><strong>5.屋内機会式</strong><br/><div class="detail">¥28,000 <span>空き: 有</span><br/>¥32,000 <span>空き: 有</span></div></div>',
    // タグ
    tag: '競合',
    // 駐車場名
    parkingLotName: '<a href="#">東京四ツ谷パーキング9</a>',
    // 最終調査日
    lastStudyDate: '2017/02/01',
    // 管理会社
    managementCompany: 'オフィスP（日本P）',
    // 総台数
    totalNumber: 20,
    // 内外
    insideAndOutside: '屋内',
    // 型式
    model: '機械式',
    // 車室判別名称
    cabinDiscriminationName: '大型HR',
    // 賃料
    rent: 100800,
    // 空き
    free: '有',
    // 全長
    fullLength: 5400,
    // 全幅
    fullWidth: 2300,
    // 全高
    totalHeight: 1800,
    // 重量
    weight: 2500,
    // 利用時間制限
    usageTimeLimit: 'MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字',
  },
  {
    id: 16,
    position: { lat: 33.2261416, lng: 131.5854192 },
    html: '<div><strong>1.屋内機会式</strong><br/><div class="detail">¥28,000 <span>空き: 有</span><br/>¥32,000 <span>空き: 有</span></div></div>',
    // タグ
    tag: '要再調査',
    // 駐車場名
    parkingLotName: '<a href="#">東京四ツ谷パーキングA</a>',
    // 最終調査日
    lastStudyDate: '2017/03/01',
    // 管理会社
    managementCompany: 'オフィスP（日本P）',
    // 総台数
    totalNumber: 20,
    // 内外
    insideAndOutside: '屋内',
    // 型式
    model: '機械式',
    // 車室判別名称
    cabinDiscriminationName: '大型HR',
    // 賃料
    rent: 100800,
    // 空き
    free: '有',
    // 全長
    fullLength: 5400,
    // 全幅
    fullWidth: 2300,
    // 全高
    totalHeight: 1800,
    // 重量
    weight: 2500,
    // 利用時間制限
    usageTimeLimit: 'MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字',
  },
  {
    id: 17,
    position: { lat: 33.1203903, lng: 131.6904759 },
    html: '<div><strong>2.屋内機会式</strong><br/><div class="detail">¥28,000 <span>空き: 有</span><br/>¥32,000 <span>空き: 有</span></div></div>',
    // タグ
    tag: '競合',
    // 駐車場名
    parkingLotName: '<a href="#">東京四ツ谷パーキングC</a>',

    // 最終調査日
    lastStudyDate: '2017/05/01',
    // 管理会社
    managementCompany: 'オフィスP（日本P）',
    // 総台数
    totalNumber: 20,
    // 内外
    insideAndOutside: '屋内',
    // 型式
    model: '機械式',
    // 車室判別名称
    cabinDiscriminationName: '大型HR',
    // 賃料
    rent: 100800,
    // 空き
    free: '有',
    // 全長
    fullLength: 5400,
    // 全幅
    fullWidth: 2300,
    // 全高
    totalHeight: 1800,
    // 重量
    weight: 2500,
    // 利用時間制限
    usageTimeLimit: 'MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字',
  },
  {
    id: 18,
    position: { lat: 33.430498,  lng:131.6047253 },
    html: '<div><strong>3.屋内機会式</strong><br/><div class="detail">¥28,000 <span>空き: 有</span><br/>¥32,000 <span>空き: 有</span></div></div>',
    // タグ
    tag: '要再調査',
    // 駐車場名
    parkingLotName: '<a href="#">東京四ツ谷パーキングf</a>',
    // 最終調査日
    lastStudyDate: '2017/09/01',
    // 管理会社
    managementCompany: 'オフィスP（日本P）',
    // 総台数
    totalNumber: 20,
    // 内外
    insideAndOutside: '屋内',
    // 型式
    model: '機械式',
    // 車室判別名称
    cabinDiscriminationName: '大型HR',
    // 賃料
    rent: 100800,
    // 空き
    free: '有',
    // 全長
    fullLength: 5400,
    // 全幅
    fullWidth: 2300,
    // 全高
    totalHeight: 1800,
    // 重量
    weight: 2500,
    // 利用時間制限
    usageTimeLimit: 'MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字',
  },
  {
    id: 19,
    position: { lat: 33.334149, lng:131.60179 },
    html: '<div><strong>4.屋内機会式</strong><br/><div class="detail">¥28,000 <span>空き: 有</span><br/>¥32,000 <span>空き: 有</span></div></div>',
    // タグ
    tag: '競合',
    // 駐車場名
    parkingLotName: '<a href="#">東京四ツ谷パーキングX</a>',
    // 最終調査日
    lastStudyDate: '2017/02/01',
    // 管理会社
    managementCompany: 'オフィスP（日本P）',
    // 総台数
    totalNumber: 20,
    // 内外
    insideAndOutside: '屋内',
    // 型式
    model: '機械式',
    // 車室判別名称
    cabinDiscriminationName: '大型HR',
    // 賃料
    rent: 100800,
    // 空き
    free: '有',
    // 全長
    fullLength: 5400,
    // 全幅
    fullWidth: 2300,
    // 全高
    totalHeight: 1800,
    // 重量
    weight: 2500,
    // 利用時間制限
    usageTimeLimit: 'MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字',
  },
  {
    id: 20,
    position: { lat: 33.1326422, lng:131.6039087 },
    html: '<div><strong>5.屋内機会式</strong><br/><div class="detail">¥28,000 <span>空き: 有</span><br/>¥32,000 <span>空き: 有</span></div></div>',
    // タグ
    tag: '競合',
    // 駐車場名
    parkingLotName: '<a href="#">東京四ツ谷パーキング9</a>',
    // 最終調査日
    lastStudyDate: '2017/02/01',
    // 管理会社
    managementCompany: 'オフィスP（日本P）',
    // 総台数
    totalNumber: 20,
    // 内外
    insideAndOutside: '屋内',
    // 型式
    model: '機械式',
    // 車室判別名称
    cabinDiscriminationName: '大型HR',
    // 賃料
    rent: 100800,
    // 空き
    free: '有',
    // 全長
    fullLength: 5400,
    // 全幅
    fullWidth: 2300,
    // 全高
    totalHeight: 1800,
    // 重量
    weight: 2500,
    // 利用時間制限
    usageTimeLimit: 'MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字MAX255文字',
  },
]
