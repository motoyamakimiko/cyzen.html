$(document).ready(function() {

  // Google Map
  var Map = null;

  // マーカーを保持
  var MarkerArray = {};

  // UI init -------------------------------------------------------------------

  $('.map-btn').attr('disabled', 'disabled');

  setTableData();
  setUI();

  /**
   * テーブルの調整のみに使用
   */
  function $E(name){ return document.getElementById(name); }
  function scroll() {
    // データ部のスクロールをヘッダに反映
    $E("header_h").scrollLeft= $E("data").scrollLeft;
    // データ部のスクロールをヘッダに反映
    $E("header_v").scrollTop = $E("data").scrollTop;
  }
  $E("data").onscroll=scroll;

  // Global Function -----------------------------------------------------------

  /**
   * テーブルのデータをセット
   */
  function setTableData() {
    $('#data table tr:not(:first-child)').remove();
    $('#header_v table tr:not(:first-child)').remove();
    for (var i = 0; i < MapDataArray.length; i++) {

      // MapDataArray data.jsで定義
      var data = MapDataArray[i];
      var tr = $('#data table tr:first-child').clone();
      tr.css('display', '');
      for (var key in data) {
        tr.find('[data-sort-key=' + key + ']').html(data[key])
      }
      $('#data table').append(tr);

      tr = $('#header_v table tr:first-child').clone();
      tr.find('.number').html(data.id);
      tr.css('display', '');

      // チェックボックスの on / off
      if (data.isSelect && data.isSelect == true) {
        tr.find('input[type="checkbox"]').prop('checked', true);
      } else {
        tr.find('input[type="checkbox"]').prop('checked', false);
      }

      // 番号イベント
      tr.find('.number').click(function(e) {
        var checkbox = $(this).parents('tr').find('input[type="checkbox"]');
        checkbox.prop('checked', !checkbox.prop('checked'));
        checkbox.trigger("change");
      });

      // チェックボタンのイベント
      tr.find('input[type="checkbox"]').change(function() {
        var id = $(this).parents('tr').find('.number').html();

        var marker = MarkerArray[id];

        var index = $(this).parents('tr').index();

        if ($(this).is(':checked')) {
          MapDataArray[index - 1].isSelect = true;
          marker.setIcon('img/num-icon/' + (Number(id)) + '.png')
        } else {
          MapDataArray[index - 1].isSelect = false;
          marker.setIcon('img/num-icon/g' + (Number(id)) + '.png')
        }

        Map.setCenter(marker.position);
      });

      $('#header_v table').append(tr);
    }
  }

  /**
   * UIを調整
   */
  function setUI() {
    var w = $(window).width();
    if (w < 1220) {
      return;
    }
    w = w * 0.9
    $('.table-border-wrap').css('width', w);
    $('#table-wrap').css('width', w - 2);
    $('#header_h').css('width', w - 108);
    $('[name="T"]').css('width', w - 208);
    $('#data').css('width', w - 92);
    $('.bottom-wrap').css('width', w);

    // map-btn
    var table = $('.table-border-wrap').offset();
    var h = $(window).outerHeight() -  $('.bottom-wrap').outerHeight() - $('.page-header').outerHeight() - 50;

    $('.table-border-wrap').css({height: h});
    $('#header_v').css({height: h - 25});
    $('#data').css({height: h - 25});
  }

  // Event ---------------------------------------------------------------------

  /**
   * 物件表示パターン選択
   */
  $('[name="optionsRadios"]').click(function() {
    $('.map-btn').removeAttr('disabled');
  });
  /**
   * 査定地図表示ボタン クリック
   */
  $('.map-btn').click(function() {
    window.open(
      'map.html?position=' + $('#val-center-magnification').val(),
      '',
      'width='
      + $('#val-window-horizontal').val()
      + ',height='
      + $('#val-window-vertical').val());
  });
  /**
   * ソート 昇順
   */
  $('.glyphicon-arrow-up').click(function(e) {
    var sortKey = $(this).parents('th').attr('data-sort-key');
    MapDataArray.sort(function(a,b){
      if(a[sortKey]<b[sortKey]) return 1;
      if(a[sortKey]>b[sortKey]) return -1;
      // 同じ値であればidで比較
      if(a.id<b.id) return 1;
      if(a.id>b.id) return -1;
      return 0;
    });
    setTableData();
  });
  /**
   * ソート 降順
   */
  $('.glyphicon-arrow-down').click(function(e) {
    var sortKey = $(this).parents('th').attr('data-sort-key');
    MapDataArray.sort(function(a,b){
      if(a[sortKey]>b[sortKey]) return 1;
      if(a[sortKey]<b[sortKey]) return -1;
      // 同じ値であればidで比較
      if(a.id>b.id) return 1;
      if(a.id<b.id) return -1;
      return 0;
    });
    setTableData();
  });

  /**
   * Window Resize
   */
  $(window).resize(function(e) {
    setUI();
  });

  // Map -----------------------------------------------------------------------

  Map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 33.2326422, lng: 131.6039087},
    zoom: 14,
    mapTypeControl: false
  });

  for (var i in MapDataArray) {
    var obj = MapDataArray[i];
    var marker = new google.maps.Marker({
      position: obj.position,
      map: Map,
      icon: 'img/num-icon/g' + (Number(i) + 1) + '.png'
    });
    var infoWindow = new google.maps.InfoWindow({
      content: obj.html
    });

    MarkerArray[obj.id] = marker;

    marker.addListener('click', function() {});

    if (i == MapDataArray.length - 1) {
      setTimeout(function() {
        Map.setCenter({lat:33.2326422, lng: 131.6039034})
      }, 1000);
    }
  }
});
